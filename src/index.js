//import express
const express = require("express");
// import express from "express";
const db = require("./services/db");
// import db from "./services/db";

// init express
const app = express();

// basic route
app.get('/perusahaan', async (req, res) => {
    const result = await db.query(`SELECT * FROM perusahaan`)
    res.send(result);
});

app.get('/mobil', async (req, res) => {
    const result = await db.query(`SELECT * FROM mobil`)
    res.send(result);
});

app.get('/karyawan', async (req, res) => {
    const result = await db.query(`SELECT k.*, m1.nama_mobil, m2.nama_mobil,m3.nama_mobil FROM karyawan AS k, perusahaan AS p, mobil AS m1,mobil AS m2,mobil AS m3 WHERE k.id_karyawan = p.id_perusahaan AND k.id_mobil_1 = m1.id_mobil AND k.id_mobil_2 = m2.id_mobil AND k.id_mobil_2 = m3.id_mobil`)
    res.send(result);
});
// listen on port
app.listen(3000, () => console.log('Server Running at http://localhost:3000'));